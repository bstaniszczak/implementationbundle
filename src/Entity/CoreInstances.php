<?php

namespace ElanIT\ImplementationBundle\Entity;

/**
 * CoreInstances
 */
class CoreInstances
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $subdomain;

    /**
     * @var boolean
     */
    private $isEnabled = '1';

    /**
     * @var string
     */
    private $version;

    /**
     * @var \DateTime
     */
    private $lastUpdateTime;

    /**
     * @var \DateTime
     */
    private $deployedAt;

    /**
     * @var string
     */
    private $type = 'shared';


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set id
     *
     * @param integer $id
     *
     * @return CoreInstances
     */
    public function setId($id)
    {
    	$this->id=$id;
    	
    	return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CoreInstances
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set subdomain
     *
     * @param string $subdomain
     *
     * @return CoreInstances
     */
    public function setSubdomain($subdomain)
    {
        $this->subdomain = $subdomain;

        return $this;
    }

    /**
     * Get subdomain
     *
     * @return string
     */
    public function getSubdomain()
    {
        return $this->subdomain;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     *
     * @return CoreInstances
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Set version
     *
     * @param string $version
     *
     * @return CoreInstances
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set lastUpdateTime
     *
     * @param \DateTime $lastUpdateTime
     *
     * @return CoreInstances
     */
    public function setLastUpdateTime($lastUpdateTime)
    {
        $this->lastUpdateTime = $lastUpdateTime;

        return $this;
    }

    /**
     * Get lastUpdateTime
     *
     * @return \DateTime
     */
    public function getLastUpdateTime()
    {
        return $this->lastUpdateTime;
    }

    /**
     * Set deployedAt
     *
     * @param \DateTime $deployedAt
     *
     * @return CoreInstances
     */
    public function setDeployedAt($deployedAt)
    {
        $this->deployedAt = $deployedAt;

        return $this;
    }

    /**
     * Get deployedAt
     *
     * @return \DateTime
     */
    public function getDeployedAt()
    {
        return $this->deployedAt;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return CoreInstances
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
