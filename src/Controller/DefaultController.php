<?php

namespace ElanIT\ImplementationBundle\Controller;

use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use \PDO;

class DefaultController extends Controller
{
	
	
	/**
	 *
	 * @Route("getImplementation", name="implementation")
	 *
	 *
	 * @return void
	 */
	public function getImplementationAction(ContainerBuilder $containerBuilder)
	{
		//@TODO: Bosze jakie to brzytkie, ale nie mam na to pomysłu
		if((isset($_SERVER['HTTP_HOST']) && strpos($_SERVER['HTTP_HOST'],'.')!==false) || isset($_SERVER['IMPLEMENTATION'])){
			if(isset($_SERVER['IMPLEMENTATION'])){
				$sImpl = $_SERVER['IMPLEMENTATION'];
			}
			elseif((isset($_SERVER['HTTP_HOST']) && strpos($_SERVER['HTTP_HOST'],'.')!==false)){
				$aImpl = explode('.',$_SERVER['HTTP_HOST']);
				$sImpl = $aImpl[0];
			}
			
			try {
				if(file_exists(getcwd().'/../app/config/parameters.yml')!==false){
					$par = Yaml::parse(file_get_contents(getcwd().'/../app/config/parameters.yml'));
				}
				else{
					$par = Yaml::parse(file_get_contents(getcwd().'/app/config/parameters.yml'));
				}
			} catch (ParseException $e) {
				printf("Unable to parse the YAML string: %s", $e->getMessage());
			}

			$hostname = $par['parameters']['database_host'];
			$dbname = $par['parameters']['database_name'];
			$username = $par['parameters']['database_user'];
			$password = $par['parameters']['database_password'];
			
			$conn =  new \PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);
			$sql = "SELECT * FROM core_instances WHERE subdomain = :imp";
			
			$stmt = $conn->prepare($sql);
			$stmt->bindValue(':imp',$sImpl, PDO::PARAM_STR);
			$stmt->execute();
			//$stmt->debugDumpParams();
			$instances = $stmt->fetchAll();
			//print_r($instances);
			if(is_array($instances) && count($instances)>0){
				return new JsonResponse($instances[0]);
			}
		}
		else {
			return new JsonResponse(null);
		}
	}
	
	
	/**
	 *
	 * @Route("getImplementations", name="implementations")
	 *
	 *
	 * @return void
	 */
	public function getImplementationsAction(Container $container)
	{
		
		try {
			if(file_exists(getcwd().'/../app/config/parameters.yml')!==false){
				$par = Yaml::parse(file_get_contents(getcwd().'/../app/config/parameters.yml'));
			}
			else{
				$par = Yaml::parse(file_get_contents(getcwd().'/app/config/parameters.yml'));
			}
		} catch (ParseException $e) {
			printf("Unable to parse the YAML string: %s", $e->getMessage());
		}
		
		$hostname = $par['parameters']['database_host'];
		$dbname = $par['parameters']['database_name'];
		$username = $par['parameters']['database_user'];
		$password = $par['parameters']['database_password'];
		
		$conn =  new \PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);
		$sql = "SELECT * FROM core_instances ORDER BY name DESC";
		
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		$instances = $stmt->fetchAll();
		if(is_array($instances)){
			return new JsonResponse($instances);
		}
		else{
			return new JsonResponse(NULL);
		}
	}
}
